import pandas
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from pdb_chooser import DlgFileChooser
from biopandas.pdb import PandasPdb
ppdb = PandasPdb()

class Main_control():


    def __init__(self):

        # Preparación de interfaz de glade
        self.builder = Gtk.Builder()
        self.builder.add_from_file("interfaz_prot.glade")

        # Carga de ventana principal para visualización
        self.window = self.builder.get_object("main_core")
        self.window.connect("destroy", Gtk.main_quit)
        self.window.set_default_size(700, 650)

        # Carga de acciones disponibles de interfaz
        open_btn = self.builder.get_object("btn_open_pdb")
        save_btn = self.builder.get_object("btn_save_pdb")
        info_app_btn = self.builder.get_object("btn_info_app")
        self.pandastree = self.builder.get_object("pandas_tree")

        #
        open_btn.connect("clicked", self.prot_filechooser, "open")
        save_btn.connect("clicked", self.prot_filechooser, "save")

        self.combobox = self.builder.get_object("combobox")
        render_text = Gtk.CellRendererText()
        self.combobox.pack_start(render_text, True)
        self.combobox.add_attribute(render_text, "text", 0)
        self.modelo = Gtk.ListStore(str)

        opciones = ["ATOM", "HETATM", "ANISOU"]

        for opt in opciones:
            self.modelo.append([opt])

        self.combobox.set_model(self.modelo)


        self.window.show_all()


    # Abrir filechooser para busqueda de archivo pdb
    def prot_filechooser(self, btn = None, option = None):


        filechooser = DlgFileChooser(option = option)
        dialogo = filechooser.dialogo

        if option == "open":
            #filtro archivos PDB
            filter_pdb = Gtk.FileFilter()
            filter_pdb.set_name("PBD Files Only")
            filter_pdb.add_pattern("*.pdb")
            dialogo.add_filter(filter_pdb)

        result = dialogo.run()

        if result == Gtk.ResponseType.OK:

            self.load_pandas_tree(dialogo.get_filename())

        elif result == Gtk.ResponseType.ACCEPT:
            self.save_guardar_pdb(dialogo.get_filename())

        dialogo.destroy()

    # Muestra de contenido #falta acción de combobox
    # Para muestra de contenido solicitado
    def load_pandas_tree(self, pdb_file):

        data = ppdb.read_pdb(pdb_file)
        print(data)
        return False 
        """
        if self.pandastree.get_columns():
            print(self.pandastree.get_columns())
            for column in self.pandastree.get_columns():
                self.pandastree.remove_column(column)

        len_columns = len(data.columns)
        model = Gtk.Listore(*(len(columns * [str])))

        self.pandastree.set_model(model = model)

        cell = Gtk.CellRendererText()

        for item in range(len_columns):
            column = Gtk.TreeViewColumn(data.columns[item],
                                        cell,
                                        text=item)
            self.pandastree.append_column(column)

        for value in data.values:
            row = [str(x) for x in value]
            model.append(row)
        """
        # Guardar archivo
        #def guardar_pdb(self, pdb_file):

        #    with open(pdb_file, "w") as newfile:


# Llamado a ventana principal
if __name__ == "__main__":
    Main_control()
    Gtk.main()
