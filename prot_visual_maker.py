#  !/usr/bin/env python3
#   -*- coding:utf-8 -*-

import __main__
__main__.pymol_argv = [" pymol ", "-qc"] # noqa : E402
from main_control import path, proteina, valor_combobox



import pymol

pymol.finish_launching()
def gen_image(proteina, path):
    aux = proteina.split(".")
    pdb_name = aux [0]
    png_file = "".join([path])
    pdb_file = protein
    pymol.cmd.load(path, protein)
    pymol.cmd.disable("all")
    pymol.cmd.enable(protein)
    pymol.cmd.hide("all")
    pymol.cmd.show("cartoon")
    pymol.cmd.set("ray_opaque_background", 0)
    pymol.cmd.pretty(protein)
    pymol.cmd.png(protein)
    pymol.cmd.ray()
    print("listo")

# Falta manejo de guardado acorde a selección del usuario
from biopandas.pdb import PandasPdb
ppdb = PandasPdb()
ppdb.read_pdb(path + protein)
print(ppdb.df.keys())
print("\ nRaw PDB file contents :\n\n %s\n..." % ppdb . pdb_text [:1000])
ppdb.to_pdb(path = "./PDBprot/6zxh_ATOM.pdb",
records = ["ATOM"],
gz = False,
append_newline = True )
gen_image(proteina, filename, path)
